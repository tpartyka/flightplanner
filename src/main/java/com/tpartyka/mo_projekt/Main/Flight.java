package com.tpartyka.mo_projekt.Main;

import com.tpartyka.mo_projekt.Airport.Airport;
import com.tpartyka.mo_projekt.Airport.AirportManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Tomasz Partyka, tpartyka@loken.pl
 */

public class Flight {
    
    private final int speed;
    
    private final Airport start_airport;
    private final Airport end_airport;
    
    private final int airport_distance;
    
    private List<Airport> reservedAirports;
    private final Date start_hour;
    
    private final float[] main = new float[3];
    private final float[] start = new float[3];
    private final float[] end = new float[3];
    
    public Flight(int speed, Airport start_airport, Airport end_airport, int airport_distance, Date start_hour) {
        this.speed = speed;
        this.start_airport = start_airport;
        this.end_airport = end_airport;
        this.airport_distance = airport_distance;
        this.start_hour = start_hour;
        this.reservedAirports = this.calculateReservedAirports();
    }
    
    public float getDistance() {
        double lat = start_airport.getLatitude()-end_airport.getLatitude();
        double lon = start_airport.getLongtitude()-end_airport.getLongtitude();
        
        return (float)(111 * Math.sqrt(Math.pow(lat, 2)+Math.pow(lon,2)));
    }
    
    public float getDistance(float _lat, float _lon){
        double lat = start_airport.getLatitude() - _lat;
        double lon = start_airport.getLongtitude() - _lon;
        
        return (float)(111 * Math.sqrt(Math.pow(lat, 2)+Math.pow(lon,2)));
    }
    
    public Date getArrivalTime(){
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        float time = (float)(this.getDistance() / this.speed * 3600 * 1000); // w milisekundach
        String output_date = dateFormat.format(this.start_hour.getTime() + (long)time);
        
        try {
            return dateFormat.parse(output_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }  
            return null;
    }
    
    private Date getArrivalTime(float distance){
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        float time = (float)(distance / this.speed * 3600 * 1000); // w milisekundach
        String output_date = dateFormat.format(this.start_hour.getTime() + (long)time);
        
        try {
            return dateFormat.parse(output_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }  
            return null;
    }
    
    public List<Airport> getReservedAirports(){
        return reservedAirports;
    }
    
    private List<Airport> calculateReservedAirports(){        
        List<Airport> out = new ArrayList();
        
        // Równanie prostej lotu Ax+By+C=0
        main[0] = (float)((end_airport.getLongtitude()  -start_airport.getLongtitude())/(end_airport.getLatitude()-start_airport.getLatitude())); // A
        main[1] = -1; // B
        main[2] = (float)(end_airport.getLongtitude() - main[0] * end_airport.getLatitude()); // C
        //System.out.printf("%f %f %f\n", main[0], main[1], main[2]);
        
        // Równanie prostej prostopadłej przechodzącej przez początkowe lotnisko Ax+By+C=0        
        start[0] = -(1/main[0]); // A
        start[1] = -1; // B
        start[2] = (float)(-start[0] * start_airport.getLatitude() - start[1] * start_airport.getLongtitude()); // C
        //System.out.printf("%f %f %f\n", start[0], start[1], start[2]);
        
        // Rownanie prostej prostopadłej przechodzącej przez końcowe lotnisko Ax+By+C=0
        end[0] = -(1/main[0]); // A
        end[1] = -1; // B
        end[2] = (float)(-end[0] * end_airport.getLatitude() - end[1] * end_airport.getLongtitude()); // C
        //System.out.printf("%f %f %f\n", end[0], end[1], end[2]);
        
        /* Dla każdego lotniska sprawdzana jest odległość od trzech prostych:
            a) prostej pokrywającej się z torem lotu,
            b) prostej prostopadłej do prostej a) przechodzącej przez punkt lotniska początkowego,
            c) prostej prostopadłej do prostej a) przechodzącej przez punkt lotniska końcowego
        
            Dodatkowo odrzucane są lotniska które nie posiadają kodu IATA.       
        */
        
        double lat, lon;
        
        for(Airport ar: new AirportManager().getAirports()){
            lat = ar.getLatitude();
            lon = ar.getLongtitude();
            
            if(((Math.abs(main[0]*lat + main[1] * lon + main[2]))/(Math.sqrt(Math.pow(main[0], 2)+ Math.pow(main[1], 2)))) * 111 <= this.airport_distance){
                if(((Math.abs(start[0]*lat + start[1] * lon + start[2]))/(Math.sqrt(Math.pow(start[0], 2)+ Math.pow(start[1], 2)))) * 111 <= this.getDistance()){
                    if(((Math.abs(end[0]*lat + end[1] * lon + end[2]))/(Math.sqrt(Math.pow(end[0], 2)+ Math.pow(end[1], 2)))) * 111  <= this.getDistance()){
                        if(!ar.getIATA_code().isEmpty()){
                           // if(this.getArrivalTime().after(this.getReserveAirportTime(ar))) {
                                out.add(ar);
                           // }                          
                        }
                    }                   
                }
            }
        }      
        return out;
    }  
    
    public Date getReserveAirportTime(Airport ar){
       
        // Generowanie prostej prostopadłej do linii lotu przechodzącej przez lotnisko ar
        
        float a,b,c;
        
        a = -(1/main[0]);
        b = -1;
        c = (float)(-a * ar.getLatitude() - b * ar.getLongtitude());
        
        float lat_pos, lon_pos;
        
        // Znalezienie pozycji przecięcia dwóch prostych
                
        lat_pos = (c-main[2])/(main[0]-a);
        lon_pos = a * lat_pos + c;
        
        return getArrivalTime(getDistance(lat_pos,lon_pos));
    }
       
}