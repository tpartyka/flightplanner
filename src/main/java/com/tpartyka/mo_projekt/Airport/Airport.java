/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tpartyka.mo_projekt.Airport;

import com.googlecode.jcsv.annotations.MapToColumn;
import java.util.Objects;

/**
 *
 * @author Tomasz Partyka, tpartyka@loken.pl
 */

public class Airport {
    
    @MapToColumn(column=0)
    private int id;
    
    @MapToColumn(column=1)
    private String name;
    @MapToColumn(column=2)
    private String city;
    @MapToColumn(column=3)
    private String country;
    
    @MapToColumn(column=4)
    private String IATA_code;
    @MapToColumn(column=5)
    private String ICAO_code;
    
    @MapToColumn(column=6)
    private double longtitude;
    @MapToColumn(column=7)
    private double latitude;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getIATA_code() {
        return IATA_code;
    }

    public String getICAO_code() {
        return ICAO_code;
    }

    public double getLongtitude() {
        return longtitude;
    }

    
    public double getLatitude() {
        return latitude;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Airport other = (Airport) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.city, other.city)) {
            return false;
        }
        if (!Objects.equals(this.country, other.country)) {
            return false;
        }
        if (!Objects.equals(this.IATA_code, other.IATA_code)) {
            return false;
        }
        if (!Objects.equals(this.ICAO_code, other.ICAO_code)) {
            return false;
        }
        if (Double.doubleToLongBits(this.longtitude) != Double.doubleToLongBits(other.longtitude)) {
            return false;
        }
        if (Double.doubleToLongBits(this.latitude) != Double.doubleToLongBits(other.latitude)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.IATA_code + " " + this.city + "\t\t" + this.country + "\t\t" + this.longtitude + "\t\t" + this.latitude;
    }
   
    public Airport(){}
}
