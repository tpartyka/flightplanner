package com.tpartyka.mo_projekt.Main;


import com.tpartyka.mo_projekt.Airport.Airport;
import com.tpartyka.mo_projekt.Airport.AirportManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * @author Tomasz Partyka, tpartyka@loken.pl
 *
 */

public class Init
{
    
    public static void main(String[] args) throws Exception
    {   
        AirportManager a = new AirportManager();
        
        int reach,speed;
        Airport start, end;
        
        if(args[0].length() == 3 && args[1].length() == 3){
            start = a.findAirport(args[0]);
            end = a.findAirport(args[1]);
        } else {
            throw new Exception("Invalid airport codes length. Expected 3.");
        }
                        
        reach = Integer.parseInt(args[2]);
        speed = Integer.parseInt(args[3]);
             
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            Date date = sdf.parse(args[4]);
            
            Flight f = new Flight(speed, start, end, reach, date);
            
            System.out.println("Total distance: " + f.getDistance() + " km.");
            System.out.println("Arrival time: " + sdf.format(f.getArrivalTime()));
            System.out.println();

            for(Airport ar : f.getReservedAirports()){
                System.out.format("%-4s%-30s%-24s%12.4f%12.4f%20s%n", ar.getIATA_code(), ar.getCity(), ar.getCountry(), ar.getLongtitude(), ar.getLatitude(), sdf.format(f.getReserveAirportTime(ar)));
            }
            
        } catch (ParseException e) {
            throw new Exception("Unsupported date format!");         
        }
       
        
        
        
    }
}
