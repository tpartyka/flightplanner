/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tpartyka.mo_projekt.Airport;

import com.googlecode.jcsv.CSVStrategy;
import com.googlecode.jcsv.annotations.internal.ValueProcessorProvider;
import com.googlecode.jcsv.reader.CSVEntryParser;
import com.googlecode.jcsv.reader.CSVReader;
import com.googlecode.jcsv.reader.internal.AnnotationEntryParser;
import com.googlecode.jcsv.reader.internal.CSVReaderBuilder;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

/**
 *
 * @author tpartyka
 */

public class AirportManager {
    private List<Airport> Airports;
    
    public AirportManager() {
        try {
            CSVEntryParser<Airport> entryParser = new AnnotationEntryParser(Airport.class, new ValueProcessorProvider());
            Reader reader = new BufferedReader(new InputStreamReader(new FileInputStream("src/main/resources/airports.dat")));
            CSVReader<Airport> csvAirportReader = new CSVReaderBuilder<Airport>(reader).strategy(CSVStrategy.UK_DEFAULT).entryParser(entryParser).build();
            this.Airports = csvAirportReader.readAll();
        } catch (IOException e){
            e.printStackTrace();
        }
            
    }
    
    public List<Airport> getAirports(){
        return Airports;
    }
    
    public Airport findAirport(String s) throws Exception{
        for(Airport a : Airports){
            if(a.getIATA_code().equalsIgnoreCase(s)){
                return a;
            }
        }
        throw new Exception("Nie ma takiego lotniska!");
    }
    
}